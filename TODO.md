# TODO #

* [x] Get Moniter ID
  * [x] Set `NTDDI_VERSION` to `NTDDI_WIN8`.
* [x] Get path to wallpaper
* [ ] Write it to a file
* [ ] Write it with the correct file ending


# Potential Issues #

* [ ] `IDesktopWallpaper::GetWallpaper` returns an empty stirng if the
  Wallpaper is a solid color. This should be detected and dealt with, so
  that the solid color is returned to the user (they shall know nothing of
  our troubles)

# Additional Features #

* [ ] Get the Wallpaper of each Monitor\
      This will require `IDesktopWallpaper::GetDevicePathCount`

