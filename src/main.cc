#include <iostream>    // cout
#include <fstream>  // ofstream, ifstream
#include <string>   // getline
#include "targetver.h"

#include <atlbase.h>    // CComPtr
#include <combaseapi.h> // CoCreateInstance(), CoInitialize()
#include <shobjidl.h>  // IDesktopWallpaper

int main (int argc, char** args)
{
    CoInitialize (NULL);
    // Create a DesktopWallpaper object
    CComPtr<IDesktopWallpaper> spdw;
    CoCreateInstance (CLSID_DesktopWallpaper, nullptr, CLSCTX_ALL,
            IID_PPV_ARGS(&spdw));

    LPWSTR monitorID;   // Buffer for moniter ID
    HRESULT hreslt; // Variable for storing winapi results temporarily

    // Get ID of primary monitor
    hreslt = spdw->GetMonitorDevicePathAt (0, &monitorID);

    switch (hreslt)
    {
        case S_OK:
        {
            std::cout << "Monitor ID acquired" << std::endl;
            break;
        }
        case E_POINTER:
        {
            std::cout << "Monitor ID was nullptr" << std::endl;
            return 1;
            break;
        }
        default:
        {
            std::cout << "Mystery error" << std::endl;
            return 1;
            break;
        }
    }

    LPWSTR wallptr;   // Buffer for wallpaper

    // Get Wallpaper of primary display
    hreslt = spdw->GetWallpaper (monitorID, &wallptr);

    switch (hreslt)
    {
        case S_OK:
        {
            std::cout << "Path to wallpaper acquired" << std::endl;
            break;
        }
        default:
        {
            std::cout << "Mystery error" << std::endl;
            return 1;
            break;
        }
    }


    std::wcout << "Your wallpaper is here: " << wallptr << std::endl;


    std::wifstream wallpaperIn (wallptr, std::ios::binary | std::ios::in);
    std::wofstream wallpaperOut ("Wallpaper.jpg", std::ios::binary | std::ios::out);

    // Write wallpaper to file
    wallpaperOut << wallpaperIn.rdbuf();

    wallpaperIn.close();
    wallpaperOut.close();

    // Clean popential memory leaks
    free (monitorID);
    free(wallptr);
    free(spdw);

    return 0;
}

