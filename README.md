# Writing the Windows Wallpaper to a file can't be that hard, right? #

The Goal of this C++ program is to write the (or eventually all) desktop
wallpapers of a Windows PC to a file. I was motivated to write this because
a friend of mine was playing *PC Builder Simulator* and they were surprised
to see their Wallpaper in-game. I wanted to show them just how
straightforward doing that can be.

# Usage #

Download or compile an executable (see [INSTALL.md](.\INSTALL.md)) and run
it. Your wallpaper will be written to a file named `Wallpaper.jpg`.

